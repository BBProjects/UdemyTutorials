// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSObjectiveActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FPSCharacter.h"

// Sets default values
AFPSObjectiveActor::AFPSObjectiveActor()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);// Set this object to not have collision.
	RootComponent = MeshComp;// Mesh component is the root.

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);// When collided w/ pawn, simply pass through pawn. 
	SphereComp->SetupAttachment(MeshComp);// Sphere component attaches to MeshComp (root).
}

// Called when the game starts or when spawned
void AFPSObjectiveActor::BeginPlay()
{
	Super::BeginPlay();

	PlayEffects();
}

void AFPSObjectiveActor::PlayEffects()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());
}


// Call NotifyActorBeginOverlap, to activate PlayEffects whenever OtherActor overlaps with Objective actor.
void AFPSObjectiveActor::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);// Call the base 'NotifyActorBeginOverlap'. 

	PlayEffects();

	// Creating MyCharacter object to access the 'bIsCarryingObjective' bool from FPSCharacter. 
	// Casting OtherActor as an AFPSCharacter type so data types are compatible. 
	AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
	if (MyCharacter) {
		MyCharacter->bIsCarryingObjective = true;

		Destroy();// Destroy the objective when "picked up".
	}
}

