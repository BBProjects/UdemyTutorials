// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSObjectiveActor.generated.h"
// NOTE: Don't #include "Components/SphereComponent.h" here; adds to compliation time. 

class USphereComponent;// Forward declare USphereComponent so compiler know what it is. 

UCLASS()
class FPSGAME_API AFPSObjectiveActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSObjectiveActor();

protected:
	UPROPERTY(VisibleAnywhere, Category = "Components")// Exposes Mesh Component anywhere.
	UStaticMeshComponent* MeshComp;// Something to visually represent the objective.

	UPROPERTY(VisibleAnywhere, Category = "Components")// Exposes Sphere Component anywhere.
	USphereComponent* SphereComp;// Used for collision. 
	
	UPROPERTY(EditDefaultsOnly, Category = "Effects")// Expose to BP's
	UParticleSystem* PickupFX;// VAriable to hold particle fx

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void PlayEffects();

public:	

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
};
